class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :name
      t.integer :duration
      t.date :last_set
      t.date :next_set

      t.timestamps
    end
  end
end
