class CoursesController < ApplicationController

  before_action :load_model, only: %i[edit update show destroy]

  def index
    @courses = Course.order(:name)
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.new course_params
    if @course.save
      redirect_to courses_path, notice: 'Курс создан успешно'
    else
      render :new
    end
  end

  def update
    if @course.update course_params
      redirect_to courses_path
    else
      render :edit
    end
  end

  private

  def load_model
    @course = Course.find params[:id]
  end

  def course_params
    params.require(:course).permit(:name, :duration, :last_set, :next_set)
  end

end
