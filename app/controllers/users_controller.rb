class UsersController < ApplicationController

  before_action :load_model, only: %i[edit update show destroy]

  def index
    @users = User.order(:name)
  end

  def show
    @user = current_user
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new user_params
    if @user.save
      redirect_to users_path, notice: "Пользователь создан успешно"
    else
      render :new
    end
  end

  def update
    if @user.update user_params
      redirect_to users_path
    else
      render :edit
    end
  end

  private

  def load_model
    @user = User.find params[:id]
  end

  def user_params
    params.require(:user).permit(:name, :email, :contacts, :comment)
  end

end
